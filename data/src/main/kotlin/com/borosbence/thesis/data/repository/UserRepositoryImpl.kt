package com.borosbence.thesis.data.repository

import android.content.Context
import com.borosbence.thesis.data.datastore.factory.UserDataStoreFactory
import com.borosbence.thesis.domain.datastore.UserDataStore
import com.borosbence.thesis.domain.model.User
import com.borosbence.thesis.domain.repository.UserRepository
import io.reactivex.Observable

class UserRepositoryImpl(context: Context) : UserRepository {

    private val userDataStore: UserDataStore = UserDataStoreFactory(context).getDataStore() as UserDataStore

    override fun getAll(): Observable<List<User>> {
        return userDataStore.getAll()
    }
}
