package com.borosbence.thesis.data.datastore.core

import android.content.Context
import com.borosbence.thesis.domain.datastore.factory.DataStoreFactory

abstract class DataStoreFactoryBase<T>(context: Context) : DataStoreFactory<T>(
        NetworkManagerImpl(context)
)