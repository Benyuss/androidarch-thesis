package com.borosbence.thesis.data.mapper

import com.borosbence.thesis.api.mapper.Mapper
import com.borosbence.thesis.domain.model.User
import com.borosbence.thesis.network.model.UserResponse

class UserResponseMapper : Mapper<UserResponse, User>() {

    override fun mapFrom(from: UserResponse): User {
        return User(
                from.name.first ?: "",
                from.name.last ?: "",
                from.email ?: "",
                from.picture.picture ?: ""
        )
    }
}