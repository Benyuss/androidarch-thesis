package com.borosbence.thesis.data.core

interface Constants {
    companion object {
        const val USER_COUNT_TO_DOWNLOAD = 50
    }
}