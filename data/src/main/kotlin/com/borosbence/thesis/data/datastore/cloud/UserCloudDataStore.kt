package com.borosbence.thesis.data.datastore.cloud

import com.borosbence.thesis.api.datastore.subtypes.CloudDataStore
import com.borosbence.thesis.data.core.Constants
import com.borosbence.thesis.data.mapper.UserResponseMapper
import com.borosbence.thesis.domain.datastore.UserDataStore
import com.borosbence.thesis.domain.model.User
import com.borosbence.thesis.network.controller.UserController
import io.reactivex.Observable

/**
 * It hides the Controller behind a nice generic interface
 */
class UserCloudDataStore : UserDataStore, CloudDataStore<User> {

    override fun getAll(): Observable<List<User>> {
        return UserController().getUsers(Constants.USER_COUNT_TO_DOWNLOAD).map { userWrapper ->
            UserResponseMapper().mapFromList(userWrapper.users)
        }
    }
}