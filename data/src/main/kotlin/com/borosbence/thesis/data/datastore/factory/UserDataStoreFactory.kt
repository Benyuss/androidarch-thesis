package com.borosbence.thesis.data.datastore.factory

import android.content.Context
import com.borosbence.thesis.api.datastore.DataStore
import com.borosbence.thesis.data.datastore.cloud.UserCloudDataStore
import com.borosbence.thesis.data.datastore.core.DataStoreFactoryBase
import com.borosbence.thesis.domain.model.User

class UserDataStoreFactory(context: Context) : DataStoreFactoryBase<User>(context) {

    override fun createCloudDataStore(): DataStore<User> {
        return UserCloudDataStore()
    }
}