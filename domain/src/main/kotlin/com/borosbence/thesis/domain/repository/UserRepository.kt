package com.borosbence.thesis.domain.repository

import com.borosbence.thesis.api.repository.Repository
import com.borosbence.thesis.domain.model.User

/**
 * Repository to get [User] data.
 *
 * If you want to have any specific CRUD methods, you should define these here.
 */
interface UserRepository : Repository<User>