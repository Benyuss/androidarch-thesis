package com.borosbence.thesis.domain.datastore

import com.borosbence.thesis.api.datastore.DataStore
import com.borosbence.thesis.domain.model.User

/**
 * DataStore to get [User] data.
 *
 * If you want to have any specific CRUD methods, you should define these here.
 */
interface UserDataStore : DataStore<User>