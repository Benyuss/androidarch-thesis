package com.borosbence.thesis.domain.model

data class User(
        var firstName: String,
        var lastName: String,
        var email: String,
        var profilePicture: String
)