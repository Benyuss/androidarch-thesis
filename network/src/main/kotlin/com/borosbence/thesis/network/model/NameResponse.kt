package com.borosbence.thesis.network.model

class NameResponse(
        var first: String?,
        var last: String?
)