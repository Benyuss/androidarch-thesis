package com.borosbence.thesis.network.model

import com.squareup.moshi.Json

class ProfilePictureResponse(
        @Json(name = "large")
        var picture: String?
)