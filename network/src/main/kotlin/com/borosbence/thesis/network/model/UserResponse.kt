package com.borosbence.thesis.network.model

class UserResponse(
        var name: NameResponse,
        var email: String?,
        var picture: ProfilePictureResponse
)