package com.borosbence.thesis.presentation.mapper

import com.borosbence.thesis.api.mapper.Mapper
import com.borosbence.thesis.domain.model.User
import com.borosbence.thesis.presentation.model.UserModel
import javax.inject.Inject

/**
 * Maps [User] objects into [UserModel]s.
 */
class UserToUserModelMapper @Inject constructor() : Mapper<User, UserModel>() {

    override fun mapFrom(from: User): UserModel {
        return UserModel(
                "${from.firstName} ${from.lastName}",
                from.email,
                from.profilePicture
        )
    }
}