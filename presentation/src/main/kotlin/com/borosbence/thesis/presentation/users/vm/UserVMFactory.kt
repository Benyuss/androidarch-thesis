package com.borosbence.thesis.presentation.users.vm

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.borosbence.thesis.domain.interactor.GetUsers
import com.borosbence.thesis.presentation.mapper.UserToUserModelMapper

class UserVMFactory(private val interactor: GetUsers,
                    private val mapper: UserToUserModelMapper) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return UserViewModel(interactor, mapper) as T
    }
}