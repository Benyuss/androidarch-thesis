package com.borosbence.thesis.presentation.di.module.user

import com.borosbence.thesis.domain.interactor.GetUsers
import com.borosbence.thesis.domain.repository.UserRepository
import com.borosbence.thesis.presentation.mapper.UserToUserModelMapper
import com.borosbence.thesis.presentation.users.vm.UserVMFactory
import dagger.Module
import dagger.Provides

@Module
class UserModule {

    @Provides
    fun provideGetUsers(userRepository: UserRepository): GetUsers {
        return GetUsers(userRepository)
    }

    @Provides
    fun provideUserVMFactory(getUsers: GetUsers, mapper: UserToUserModelMapper): UserVMFactory {
        return UserVMFactory(getUsers, mapper)
    }
}