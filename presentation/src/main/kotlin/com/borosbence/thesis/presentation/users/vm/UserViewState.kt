package com.borosbence.thesis.presentation.users.vm

import com.borosbence.thesis.presentation.model.UserModel

/**
 * This data class holds the state of the [com.borosbence.thesis.presentation.users.view.UserView]
 */
data class UserViewState(
        var showLoading: Boolean = true,
        var users: List<UserModel>? = null
)