package com.borosbence.thesis.presentation.users.presenter

import com.borosbence.thesis.presentation.core.Presenter
import com.borosbence.thesis.presentation.users.adapter.UserAdapter
import com.borosbence.thesis.presentation.users.vm.UserVMFactory
import com.borosbence.thesis.presentation.users.vm.UserViewModel
import com.borosbence.thesis.presentation.users.vm.UserViewState

/**
 * Presenter to control the [com.borosbence.thesis.presentation.users.view.UserView] display logic.
 */
interface UserPresenter : Presenter {

    var viewModel: UserViewModel
    var factory: UserVMFactory
    var adapter: UserAdapter

    /**
     * Pass a valid state to this method.
     * It will modify the view according to the state.
     */
    fun handleViewState(state: UserViewState)

    fun initRecyclerView()
}