package com.borosbence.thesis.presentation.di.module.user

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class UserScope