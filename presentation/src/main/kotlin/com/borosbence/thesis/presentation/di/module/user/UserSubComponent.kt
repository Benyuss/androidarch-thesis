package com.borosbence.thesis.presentation.di.module.user

import com.borosbence.thesis.presentation.users.presenter.UserPresenterImpl
import dagger.Subcomponent

@UserScope
@Subcomponent(modules = [
    UserModule::class
])
interface UserSubComponent {

    fun inject(userPresenterImpl: UserPresenterImpl)
}