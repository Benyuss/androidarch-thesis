package com.borosbence.thesis.presentation.di.module

import android.content.Context
import com.borosbence.thesis.data.repository.UserRepositoryImpl
import com.borosbence.thesis.domain.repository.UserRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule {

    @Singleton
    @Provides
    fun provideUserRepository(context: Context): UserRepository {
        return UserRepositoryImpl(context)
    }
}