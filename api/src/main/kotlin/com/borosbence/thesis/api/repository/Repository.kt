package com.borosbence.thesis.api.repository

import io.reactivex.Observable

/**
 * Repositories grants access to [com.borosbence.thesis.api.datastore.DataStore]s.
 *
 * It's a tunnel between DataStores and [com.borosbence.thesis.api.interactor.Interactor]s.
 */
interface Repository<T> {

    /**
     * Get all available objects, wrapped into an Observable.
     */
    fun getAll(): Observable<List<T>>
}