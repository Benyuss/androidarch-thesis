package com.borosbence.thesis.api.event

/**
 * An error happened in the stream.
 *
 * If you want to handle the exception just listen to this event.
 */
class RxExceptionEvent (var error: Throwable)