package com.borosbence.thesis.api.rx

import com.borosbence.thesis.api.event.RxExceptionEvent
import io.reactivex.observers.DisposableObserver

/**
 * This observer provides auto-dispose mechanism and you won't encounter any OnErrorNotImplementedException in your Rx streams if you subscribe with this type.
 * Much safer and cleaner then using lambdas only.
 */
abstract class SafeObserver<T> : DisposableObserver<T>() {

    override fun onComplete() {
        dispose()
    }

    override fun onNext(t: T) { /* Default implementation. Can be overridden. */
    }

    override fun onError(e: Throwable) {
        RxBus.publish(RxExceptionEvent(e))
        dispose()
    }
}