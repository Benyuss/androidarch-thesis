package com.borosbence.thesis.api.util

/**
 * Kotlin has a built-in singleton type which is the object keyword.
 * The only problem is, you can not have a constructor in this case.
 *
 * If you want to pass something to a singleton you may use dagger or this wrapper class.
 *
 * You can only have one constructor argument!
 *
 * Make the class constructor private.
 * Make a companion object which extends this class.
 *
 * You can get the instance with the [getInstance] method from now on.
 */
open class SingletonHolder<out T, in A>(creator: (A) -> T) {
    private var creator: ((A) -> T)? = creator
    @Volatile private var instance: T? = null

    fun getInstance(arg: A): T {
        val i = instance
        if (i != null) {
            return i
        }

        return synchronized(this) {
            val i2 = instance
            if (i2 != null) {
                i2
            } else {
                val created = creator!!(arg)
                instance = created
                creator = null
                created
            }
        }
    }
}